<?php
namespace App\Repositories;

use App\Model\Product;

class ProductRepository{

    protected $model;

    public function __construct(Product $product){
        $this->model = $product;
    }

    public function findProduct()
    {
        return $this->model->all();
    }

    public function findProductById($id)
    {
        return $this->model->where("id", $id)->get();
    }

}