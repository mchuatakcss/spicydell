<?php
namespace App\Repositories;

use App\Model\Category;

class UserRepository{

    protected $model;

    public function __construct(Category $category){
        $this->model = $category;
    }

    public function findById($id)
    {
        $user =  $this->model->where("id", $id)->get();
        return  $user;
    }
}