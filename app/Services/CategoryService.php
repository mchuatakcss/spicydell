<?php
namespace App\Services;

use App\Http\Controllers\Controller;

use App\Repositories\CategoryRepository;

class CategoryService extends Controller{

    private $categoryRepo;

    public function __construct(CategoryRepository $categoryRepo){
        $this->categoryRepo = $categoryRepo;
    }
    

    public function findById($id){

        $user = $this->categoryRepo->findById($id);

        if (!empty($user)) {
            return $user;
        }
    }

}