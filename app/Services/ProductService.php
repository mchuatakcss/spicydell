<?php
namespace App\Services;

use App\Http\Controllers\Controller;

use App\Repositories\ProductRepository;

class ProductService extends Controller{

    private $productRepo;

    public function __construct(ProductRepository $productRepo){
        $this->productRepo = $productRepo;
    }
    
    public function findProduct(){

        $product = $this->productRepo->findProduct();
        
        if (!empty($product)) {
            return $product;
        }
    }

    public function findProductById($id){

        $product = $this->productRepo->findProductById($id);
        
        if (!empty($product)) {
            return $product;
        }
    }

}