<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\ProductService;

class ProductController extends Controller
{
    private $productService;

    public function __construct(ProductService $productService){
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $response = $this->productService->findProduct();

        if(!empty($response)) {
            return $this->sendResponse($response, 200); 
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $response = $this->productService->findProductById($id);

        if(!empty($response)) {
            return $this->sendResponse($response, 200); 
        }
        
    }

}
